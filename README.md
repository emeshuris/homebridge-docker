# a working copy of homebridge

## please keep in mind, my configuration dir is symlinked into another directory

### the contents of that directory is my config.json and run file

`

# !/bin/bash

cp /root/avahi/avahi-daemon.conf /etc/avahi/

cd /root/config

dbus-daemon --system service dbus enable avahi-daemon -D

service dbus stop service avahi-daemon stop

service dbus restart service avahi-daemon start

homebridge

`
